#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "Shapes/Rectangle.h"

class Obstacle
{
public:
	Obstacle(Scene* parentScene, Vec2 position, float width, float height_rock, float height_shadow)
	{
		float half_rock_height = height_rock * 0.5f;
		float half_shadow_height = height_shadow * 0.5f;
		Vec2 shadowposition = position + Vec2(0, -1) * (half_rock_height + half_shadow_height);

		rock = new C_Rectangle(parentScene, position, Vec2(0, 0), width, height_rock, Color4F::GRAY);
		shadow = new C_Rectangle(parentScene, shadowposition, Vec2(0, 0), width, height_shadow, Color4F(0.1f, 0.1f, 0.1f, 0.7f));
	}
	bool checkHidden(Vec2 position)
	{
		bool inside_x = (position.x >= shadow->getLeftX() && position.x <= shadow->getRightX());
		bool inside_y = (position.y >= shadow->getBottomY() && position.y <= shadow->getTopY());

		return (inside_x && inside_y);
	}
private:
	C_Rectangle* rock;
	C_Rectangle* shadow;
};
