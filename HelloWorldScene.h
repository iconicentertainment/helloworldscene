#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "Enemy.h"
#include "Player.h"
#include "Projectile.h"
#include "SimpleAudioEngine.h"
#include "cocos2d.h"
#include "DisplayHandler.h"
#include "InputHandler.h"
#include "Controller.h"
#include "Searcher.h"
#include "Obstacle.h"
#include <vector>
#include <cmath>
#include <random>

class HelloWorld : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);
	void update(float deltaTime);
	void Controls();
	void spawnEnemy();
	void spawnShuriken();

	float xCenter;
	float yCenter;
	// implement the "static create()" method manually
	CREATE_FUNC(HelloWorld);

	xInput::XBoxInput XBoxController;

	Director* director;
	Vec2 windowSize;

	Searcher* S1;
	//Obstacle* Ob1;
	Player _player;
	vector <Enemy> enemyList;

	Rect boundry;

	Sprite* building1;
	Sprite* building2;

	Sprite* rickshaw;
	Sprite* ramp;

	Sprite* trap1;
	Sprite* trap2;
	Sprite* trap3;

	Sprite* bush1;
	Sprite* bush2;
	Sprite* bush3;

	Sprite* healthBar;
	Sprite* healthBar2;

	Sprite* introBG;
	Label* label;
	Label* label2;

	Sprite* bg;
	Sprite* bg2;
	Sprite* bg3;

	Sprite* tree;
	Sprite* tree2;
	Sprite* tree3;

	Sprite* heart;

	Sprite* ControllerJump;
	Sprite* ControllerMovement;
	Sprite* ControllerAttack;
	Sprite* ControllerDash;

	Sprite* grass;
	Sprite* grass2;
	Sprite* grassBottom;
	Sprite* grassBottom2;

	//BOSS STUFF
	Sprite* bossSprite;
	PhysicsBody* bossBody;
	int bossHealth = 500;

	Vector<SpriteFrame*> animFramesBossStand;
	Animation* animationBossStand;
	Animate* BossStand;
	
	vector <Sprite> grassList;
	vector <Sprite> grassBottomList;

	vector<Projectile> shurikenList;

	Sprite* sprite;
	Sprite* crate;
	PhysicsBody* playerBody;
	PhysicsBody* hostileBody;
	
	//UI
	Sprite* bar;
	Sprite* bar2;
	Sprite* UICharacter;

	//run animation

	Vector<SpriteFrame*> animFramesRight;
	Animation* animationRight;
	Animate* runRight;

	Vector<SpriteFrame*> animFramesStand;
	Animation* animationStand;
	Animate* Stand;

	Vector<SpriteFrame*> animFramesSlash;
	Animation* animationSlash;
	Animate* Slash;

	ParticleSystemQuad* blood;
	ParticleFire* fire;

	float gameTime = 0.0f;
	float coolDownTime = 0.0f;
	float attackTime = 0.0f;
	float rumbleTime = 0.0f;
	float AssassinationTime = 0.0f;
	float throwTime = 0.0f;

	bool right;
	bool left;
	bool dash = false;
	bool jump = false;
	bool attack = false;
	bool threw = false;
	bool introDone = false;
	int shurikenLeft = 3;
	
	CocosDenshion::SimpleAudioEngine* audio;
};

#endif // __HELLOWORLD_SCENE_H__