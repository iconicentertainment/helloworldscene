#include "MainMenu.h"
#include "HelloWorldScene.h"

USING_NS_CC;

Scene* HelloWorld2::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = HelloWorld2::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld2::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();

	/*background = Sprite::create("BackGround.png");

	background->setPosition(Vec2(3595, 565));
	this->addChild(background, -100);*/


	mySprite = Sprite::create("BackGround.png");

	mySprite->setPosition(Point((visibleSize.width / 2) + origin.x, (visibleSize.height / 2) + origin.y));
	mySprite->setScale(1.09f);

	this->addChild(mySprite);

	/*auto menu_item_1 = MenuItemFont::create("Play", CC_CALLBACK_1(HelloWorld2::Play, this));
	auto menu_item_2 = MenuItemFont::create("Highscores", CC_CALLBACK_1(HelloWorld2::Highscores, this));
	auto menu_item_3 = MenuItemFont::create("Settings", CC_CALLBACK_1(HelloWorld2::Settings, this));*/
	auto menu_item_4 = MenuItemImage::create("Start_Button.png", "Start_Button2.png", CC_CALLBACK_1(HelloWorld2::ImageButton, this));
	auto menu_item_5 = MenuItemImage::create("Exit_Button.png", "Exit_Button2.png", CC_CALLBACK_1(HelloWorld2::ImageButton2, this));


	/*menu_item_1->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 5) * 4));
	menu_item_2->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 5) * 3));
	menu_item_3->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 5) * 2));*/
	menu_item_4->setPosition(Point(350, 550));
	menu_item_5->setPosition(Point(350, 450));

	auto *menu = Menu::create(/*menu_item_1, menu_item_2, menu_item_3,*/ menu_item_4, menu_item_5, NULL);
	menu->setPosition(Point(0, 0));
	this->addChild(menu);

	/*audio = CocosDenshion::SimpleAudioEngine::getInstance();
	audio->preloadBackgroundMusic("bgMusic2.mp3");
	audio->preloadBackgroundMusic("battleMusic.mp3");
	audio->playBackgroundMusic("bgMusic2.mp3");
	*/
	return true;
}

//void HelloWorld2::Play(cocos2d::Ref *pSender)
//{
//	CCLOG("Play");
//
//	auto scene = HelloWorld::createScene();
//	Director::getInstance()->pushScene(scene);
//
//}

//void HelloWorld2::Highscores(cocos2d::Ref *pSender)
//{
//	CCLOG("Highscores");
//}

//void HelloWorld2::Settings(cocos2d::Ref *pSender)
//{
//	CCLOG("Settings");
//}

void HelloWorld2::ImageButton(cocos2d::Ref *pSender)
{
	CCLOG("IMAGE Button");

	auto scene = HelloWorld::createScene();
	Director::getInstance()->pushScene(scene);
}

void HelloWorld2::ImageButton2(cocos2d::Ref *pSender)
{
	CCLOG("IMAGE Button2");
	CCDirector::sharedDirector()->end();

	if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS) exit(0);


	/*auto scene = HelloWorld::createScene();
	Director::getInstance()->pushScene(scene);*/
}


void HelloWorld2::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
	return;
#endif

	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}