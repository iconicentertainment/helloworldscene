#include "ScrollingBg.h"
#include "HelloWorldScene.h"
#include "Player.h"


ScrollingBg* ScrollingBg::create(string _name, float _speed, float _yPos)
{
	ScrollingBg* ob = new ScrollingBg();
	if (ob && ob->initScrollingBg(_name, _speed, _yPos))
	{
		ob->autorelease();
		return ob;
	}
	CC_SAFE_DELETE(ob);
	return NULL;
}

bool ScrollingBg::initScrollingBg(string _name, float _speed, float _yPos)
{
	winSize = CCDirector::sharedDirector()->getWinSize();
	speed = _speed;
	gameBg1 = CCSprite::create(_name.c_str());
	gameBg1->setPosition(Vec2(3595, 0));
	gameBg1->setAnchorPoint(Vec2(0.5, 0.0));
	gameBg1->setScaleX(1.0);
	addChild(gameBg1);
	gameBg2 = CCSprite::create(_name.c_str());
	gameBg2->setPosition(Vec2(10792, 0));
	gameBg2->setAnchorPoint(Vec2(0.5, 0.0));
	gameBg2->setScaleX(1.0);
	addChild(gameBg2);
	return true;
}

void ScrollingBg::update(int direction, float position)
{
	xInput::Stick lStick, rStick;
	XBoxController.GetSticks(0, lStick, rStick);

	ScrollingBg something;

	//float size = Director::getWinSizeInPixels();

	// scroll bg left or right
	CCPoint bg1pos = gameBg1->getPosition();
	CCPoint bg2pos = gameBg2->getPosition();

	gameBg1->setPosition(Vec2((bg1pos.x - speed*direction), bg1pos.y));
	gameBg2->setPosition(Vec2((bg2pos.x - speed*direction), bg2pos.y));

	//if (gameBg1->getPosition().x < -gameBg1->getContentSize().width)
	//if (gameBg1->getContentSize().width < position)
	if (direction = 1.0)
	{
		if (gameBg1->getPositionX() <= position - gameBg1->getContentSize().width)
			gameBg1->setPosition(Vec2(gameBg2->getPosition().x + gameBg1->getContentSize().width*direction, gameBg1->getPosition().y));
		if (gameBg2->getPositionX() <= position - gameBg1->getContentSize().width)
			gameBg2->setPosition(Vec2(gameBg1->getPosition().x + gameBg2->getContentSize().width*direction, gameBg2->getPosition().y));
	}
	if (direction = -1.0)
	{
		if (gameBg1->getPositionX() >= position + gameBg1->getContentSize().width)
			gameBg1->setPosition(Vec2(gameBg2->getPosition().x + gameBg1->getContentSize().width*direction, gameBg1->getPosition().y));
		if (gameBg2->getPositionX() >= position + gameBg1->getContentSize().width)
			gameBg2->setPosition(Vec2(gameBg1->getPosition().x + gameBg2->getContentSize().width*direction, gameBg2->getPosition().y));
	}
}