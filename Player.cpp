#include "Player.h"

Player::Player()
{
	sprite = cocos2d::Sprite::create("standing\\1.png");
	sprite->setScale(0.7f);
	sprite->setPosition(cocos2d::Vec2(300, 200));
	sprite->setAnchorPoint(cocos2d::Vec2(0, 0));

	playerBody = cocos2d::PhysicsBody::createBox(sprite->getContentSize());
	sprite->setPhysicsBody(playerBody);
	playerBody->setVelocity(cocos2d::Vec2(0, 0));
	playerBody->setRotationEnable(false);

	sprite->getPhysicsBody()->setCategoryBitmask(0x02);
	sprite->getPhysicsBody()->setCollisionBitmask(0x01);

	health = 100;
	stamina = 90;
}

void Player::setDamage(int damage)
{
	health = health - damage;
}

void Player::Attack(Enemy& enemy, int damage)
{
	enemy.setHealth(enemy.getHealth() - damage);
	stamina = stamina - 30;
}

void Player::setStamina(int _stamina)
{
	stamina = _stamina;
}
int Player::getStamina()
{
	return stamina;
}