#ifndef _ScrollingBgLayer_H
#define _ScrollingBgLayer_H
#include <iostream>
#include "ScrollingBg.h"

class ScrollingBgLayer : public CCLayer
{
public:
	ScrollingBgLayer(float speed);
	~ScrollingBgLayer();
	ScrollingBg* bglayer1;
	ScrollingBg* bglayer2;
	ScrollingBg* bglayer3;
	ScrollingBg* bglayer4;
	void update(int direction, float position);
};
#endif
