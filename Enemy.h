#ifndef ENEMY_H
#define ENEMY_H
#include "Searcher.h"
#include "Character.h"
#include "cocos2d.h"

class Enemy : public Character
{
public:
	Enemy();
//	void Attack(Player&);
	bool checkDetection();
	void setDamage(int);
	int getHealth();
	void checkDead();
	cocos2d::Sprite* sprite;
	cocos2d::PhysicsBody* enemyBody;
	Searcher* S1;


	int health;
};
#endif // !ENEMY_H
