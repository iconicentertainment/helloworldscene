#ifndef ScrollingBg_H
#define ScrollingBg_H

#include <iostream>
#include "cocos2d.h"
#include "Controller.h"

using namespace std;
using namespace cocos2d;
class ScrollingBg : public CCNode
{
public:
	static ScrollingBg* create(string name, float _speed, float _yPos);
	bool initScrollingBg(string _name, float _speed, float _yPos);
	xInput::XBoxInput XBoxController;

	CCSprite* gameBg1, *gameBg2;


	float speed;
	string mame;

	CCSize winSize;
	void update(int direction, float position);
};

#endif