#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"
#include "Enemy.h"
#include "cocos2d.h"

class Player : public Character
{
public:
	Player();
	void Attack(Enemy&, int);
	void setDamage(int);
	int getHealth();
	int getStamina();
	void setStamina(int);

	cocos2d::Sprite* sprite;
	cocos2d::PhysicsBody* playerBody;

private:
	int stamina;
	int health;
};
#endif // !PLAYER_H