#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include <iostream>
using namespace std;

class Searcher
{
public:
	Searcher(Scene* parentScene, Vec2 position, float radius)
	{
		this->position = position;
		this->radius = radius;
		this->direction = Vec2(0, 1);

		drawNode = DrawNode::create();
		//	drawNode->drawDot(Vec2(0, 0), radius, Color4F::RED);
		drawNode->drawLine(Vec2(0, radius), Vec2(0, 500), Color4F(0.5f, 0.0f, 0.0f, 1.0f));
		drawNode->drawLine(Vec2(0, radius), Vec2(-150, 500), Color4F(0.5f, 0.0f, 0.0f, 1.0f));


		toplayer = DrawNode::create();

		parentScene->addChild(toplayer);
		parentScene->addChild(drawNode);

		setPosition(position);
	}
	void setPosition(Vec2 p)
	{
		this->position = p;
		drawNode->setPosition(p);
	}
	Vec2 getPosition()
	{
		return position;
	}
	void setRotation(float degrees)
	{
		float rad = degrees * (M_PI / 180.0f);
		direction.x = sinf(rad);
		direction.y = cosf(rad);
		drawNode->setRotation(degrees);
	}
	Vec2 getDirection()
	{
		return direction;
	}
	void DrawLineToPlayer(Vec2 p)
	{
		toplayer->clear();
		toplayer->drawLine(position, p, Color4F::RED);
	}
private:
	Vec2 position;
	Vec2 direction;
	float radius;

	DrawNode* drawNode;
	DrawNode* toplayer;
};