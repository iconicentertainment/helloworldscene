#include "HelloWorldScene.h"
USING_NS_CC;

Scene* HelloWorld::createScene()
{
	Scene* scene = Scene::createWithPhysics();
	scene->addChild(HelloWorld::create());
	scene->getPhysicsWorld()->setGravity(Vec2(0, -2730));
//	scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();


	//the center of the screen
	xCenter = visibleSize.width / 2;
	yCenter = visibleSize.height / 2;

	
	if (!Scene::initWithPhysics())
	{

	}

	director = Director::getInstance();
	director->setClearColor(Color4F(255, 255, 255, 255));
	windowSize = DISPLAY->getWindowSizeAsVec2();

	
	boundry = Rect(0, 0, 20000, 1080);
	
	introBG = Sprite::create("black.jpg");
	introBG->setScale(1.5f);
	introBG->setPosition(1000, 800);
	this->addChild(introBG, 99);

	label = Label::createWithTTF("Long ago in a village on the outskirts of Japan, \nKanji the emperor ruled in peace and hopes to keep his people as safe as possible.\n\nBut the previous emperor Aki had different ideas. \n\nFeeding off of power and greed, Aki overthrew Kanji and took his place as the emperor, \nKanji fought valiantly but it was not enough to defeat Aki.\n\nNow under a new name, Aki now goes as Unmei has consumed Kanjis soul and now is near invincible. \n\nAs his time as emperor goes on it seems the village and all of Japan would soon suffer to his wrath. \n\nThat is until Etsu, the next in line as emperor, is not happy with the way Unmei is reigning supreme. \n\nHe now sets his sights on Kanjis sword to help him aid in his battle against the almighty Unmei.", "fonts/arial.ttf", 40);
	label->setPosition(Vec2(1000,600));
	this->addChild(label, 100);

	label2 = Label::createWithTTF("Press START To Continue", "fonts/arial.ttf", 30);
	label2->setPosition(Vec2(1000, 100));
	this->addChild(label2, 100);

	 // add background
	 bg = Sprite::create("bg2.png");
	 bg2 = Sprite::create("bg2.png");
	 bg3 = Sprite::create("bg2.png");
	 if (bg == nullptr)
	 {
	 	problemLoading("bg.png");
	 }
	 else
	 {
	 	// position the sprite on the center of the screen
	 	// add the sprite as a child to this layer
	 	
	 }

	 bg->setPosition(Vec2(3595, 565));
	 bg2->setPosition(Vec2(bg->getContentSize().width + 3593, 560));
	 bg3->setPosition(Vec2((bg->getContentSize().width*2) + 3593, 560));

	 this->addChild(bg, -100);
	 this->addChild(bg2, -100);
	 this->addChild(bg3, -100);
	 this->addChild(_player.sprite);

	 for (int i = 0; i < 4; i++)
	 {
		 spawnEnemy();
	 }

	 enemyList[0].sprite->setPosition(Vec2(3550, 600));

	 enemyList[1].sprite->setPosition(Vec2(5300, 600));
	 enemyList[2].sprite->setPosition(Vec2(5600, 600));

	 enemyList[3].sprite->setPosition(Vec2(11400, 600));
	 enemyList[3].sprite->getPhysicsBody()->setCategoryBitmask(0x02);
	 enemyList[3].sprite->getPhysicsBody()->setCollisionBitmask(0x02);

	 bossSprite = Sprite::create("boss-standing\\1.png");
	 bossSprite->setAnchorPoint(Vec2(0, 0));
	 bossSprite->setPosition(Vec2(16000, 600));
	
	 bossBody = PhysicsBody::createBox(bossSprite->getContentSize());
	 bossSprite->setPhysicsBody(bossBody);
	 bossBody->setVelocity(Vec2(0, 0));
	 bossBody->setRotationEnable(false);
	
	 this->addChild(bossSprite);
	
	 //Boss Animation
	  Vector<SpriteFrame*> animFramesBossStand;
	 
	  animFramesBossStand.reserve(3);
	  animFramesBossStand.pushBack(SpriteFrame::create("boss-standing\\1.png", Rect(0, 0, 251, 431)));
	  animFramesBossStand.pushBack(SpriteFrame::create("boss-standing\\2.png", Rect(0, 0, 251, 431)));
	  animFramesBossStand.pushBack(SpriteFrame::create("boss-standing\\3.png", Rect(0, 0, 251, 431)));
	 
	  BossStand = Animate::create(Animation::createWithSpriteFrames(animFramesBossStand, 0.1f));
	  bossSprite->runAction(RepeatForever::create(BossStand));


	 //UI

	 bar = Sprite::create("bar.png");
	 bar->setPosition(235, 968);
	 bar->setAnchorPoint(Vec2(0.0, 0.5));
	 bar->setScaleX(129.0f);
	 bar->setScaleY(0.70f);
	 this->addChild(bar, 9);

	 bar2 = Sprite::create("bar2.png");
	 bar2->setPosition(230, 948);
	 bar2->setAnchorPoint(Vec2(0.0, 0.5));
	 //bar2->setColor(cocos2d::Color3B::MAGENTA);
	 bar2->setScaleX(90.0f);
	 bar2->setScaleY(0.64f);
	 this->addChild(bar2, 9);

	 UICharacter = Sprite::create("UIcharacter.png");
	 UICharacter->setPosition(Vec2(50, 975));
	 UICharacter->setScale(1.5f);
	 UICharacter->setAnchorPoint(Vec2(0.0, 0.5));
	 this->addChild(UICharacter, 10);

	 //Environment
	tree = Sprite::create("tree.png");
	tree->setScale(0.8f);
	tree->setPosition(Vec2(1100, 500));
	this->addChild(tree, -1);

	tree2 = Sprite::create("tree.png");
	tree2->setScale(0.8f);
	tree2->setPosition(Vec2(14100, 500));
	this->addChild(tree2, -1);

	heart = Sprite::create("heart.png");
	heart->setScale(0.6f);
	heart->setPosition(Vec2(14300,400));
	this->addChild(heart,2);

	auto heartBody = PhysicsBody::createBox(heart->getContentSize(), PhysicsMaterial(0, 0, 2));
	heart->setPhysicsBody(heartBody);
	heartBody->setDynamic(false);
	heartBody->setGravityEnable(false);

	tree3 = Sprite::create("tree.png");
	tree3->setScale(0.8f);
	tree3->setFlipX(true);
	tree3->setPosition(Vec2(14500, 500));
	this->addChild(tree3, -1);

	grass = Sprite::create("grass2.png");
	grass->setScaleY(0.8f);
	grass->setPosition(Vec2(1700, visibleSize.height / 2 - origin.y - 150));
	this->addChild(grass,2);
	
	grass2 = Sprite::create("grass2.png");
	grass2->setScaleY(0.8f);
	grass2->setPosition(Vec2(grass->getContentSize().width + 1700, visibleSize.height / 2 - origin.y - 150));
	this->addChild(grass2,2);

	grassBottom = Sprite::create("grass.png");
	grassBottom->setScaleY(0.8f);
	grassBottom->setPosition(Vec2(1700, visibleSize.height / 2 - origin.y - 110));
	this->addChild(grassBottom, 3);

	grassBottom2 = Sprite::create("grass.png");
	grassBottom2->setScaleY(0.8f);
	grassBottom2->setPosition(Vec2(grass->getContentSize().width + 1700, visibleSize.height / 2 - origin.y - 110));
	this->addChild(grassBottom2, 3);

	auto grass3 = Sprite::create("grass2.png");
	grass3->setScaleY(0.8f);
	grass3->setPosition(Vec2((grass->getContentSize().width*2) + 1700, visibleSize.height / 2 - origin.y - 150));
	this->addChild(grass3, 2);

	auto grassBottom3 = Sprite::create("grass.png");
	grassBottom3->setScaleY(0.8f);
	grassBottom3->setPosition(Vec2((grass->getContentSize().width * 2) + 1700, visibleSize.height / 2 - origin.y - 110));
	this->addChild(grassBottom3, 3);

	auto grass4 = Sprite::create("grass2.png");
	grass4->setScaleY(0.8f);
	grass4->setPosition(Vec2((grass->getContentSize().width * 3) + 1700, visibleSize.height / 2 - origin.y - 150));
	this->addChild(grass4, 2);

	auto grassBottom4 = Sprite::create("grass.png");
	grassBottom4->setScaleY(0.8f);
	grassBottom4->setPosition(Vec2((grass->getContentSize().width * 3) + 1700, visibleSize.height / 2 - origin.y - 110));
	this->addChild(grassBottom4, 3);

	auto grass5 = Sprite::create("grass2.png");
	grass5->setScaleY(0.8f);
	grass5->setPosition(Vec2((grass->getContentSize().width * 4) + 1700, visibleSize.height / 2 - origin.y - 150));
	this->addChild(grass5, 2);

	auto grassBottom5 = Sprite::create("grass.png");
	grassBottom5->setScaleY(0.8f);
	grassBottom5->setPosition(Vec2((grass->getContentSize().width * 4) + 1700, visibleSize.height / 2 - origin.y - 110));
	this->addChild(grassBottom5, 3);

	auto grass6 = Sprite::create("grass2.png");
	grass6->setScaleY(0.8f);
	grass6->setPosition(Vec2((grass->getContentSize().width * 5) + 1700, visibleSize.height / 2 - origin.y - 150));
	this->addChild(grass6, 2);

	auto grassBottom6 = Sprite::create("grass.png");
	grassBottom6->setScaleY(0.8f);
	grassBottom6->setPosition(Vec2((grass->getContentSize().width * 5) + 1700, visibleSize.height / 2 - origin.y - 110));
	this->addChild(grassBottom6, 3);

	ControllerJump = Sprite::create("ControllerJump.png");
	ControllerJump->setScale(0.25f);
	ControllerJump->setPosition(Vec2(1850, 800));
	this->addChild(ControllerJump, 2);


	//game audio
	audio = CocosDenshion::SimpleAudioEngine::getInstance();
	audio->preloadBackgroundMusic("bgMusic2.mp3");
	audio->preloadBackgroundMusic("battleMusic.mp3");
	audio->playBackgroundMusic("bgMusic2.mp3");

	//shuriken

	/*shuriken = Sprite::create("shuriken.png");
	shuriken->setScale(0.2f);
	this->addChild(shuriken, 2);*/

	auto ground = Sprite::create("ground.png");
	ground->setScale(150.0f, 1.0f);
	ground->setPosition(Vec2(1000, 50));

	auto spriteBody = PhysicsBody::createBox(ground->getContentSize(), PhysicsMaterial(0, 0, 2));
	ground->setPhysicsBody(spriteBody);
	spriteBody->setDynamic(false);
	spriteBody->setGravityEnable(false);
	this->addChild(ground, -110);

	ground->getPhysicsBody()->setCategoryBitmask(0x03);
	ground->getPhysicsBody()->setCollisionBitmask(0x03);

	auto crate = Sprite::create("crate.png");
	crate->setScale(0.15f);
	crate->setPosition(Vec2(1850, 200));

	ground->getPhysicsBody()->setCategoryBitmask(0x03);
	ground->getPhysicsBody()->setCollisionBitmask(0x03);

	auto crateBody = PhysicsBody::createBox(crate->getContentSize(), PhysicsMaterial(0, 0, 2));
	crate->setPhysicsBody(crateBody);
	crateBody->setDynamic(false);
	crateBody->setGravityEnable(false);
	this->addChild(crate,2);
	
	auto crate2 = Sprite::create("crate.png");
	crate2->setScale(0.15f);
	crate2->setPosition(Vec2(9000, 200));
	auto crateBody2 = PhysicsBody::createBox(crate2->getContentSize(), PhysicsMaterial(0, 0, 2));
	crate2->setPhysicsBody(crateBody2);
	crateBody2->setDynamic(false);
	crateBody2->setGravityEnable(false);
	this->addChild(crate2, 2);


	bush1 = Sprite::create("bush1.png");
	bush1->setScale(2.2f, 1.8f);
	bush1->setPosition(Vec2(11000, 200));

	bush2 = Sprite::create("bush1.png");
	bush2->setScale(2.2f, 1.8f);
	bush2->setPosition(Vec2(11300, 200));

	bush3 = Sprite::create("bush1.png");
	bush3->setScale(2.2f, 1.8f);
	bush3->setPosition(Vec2(11600, 200));

	this->addChild(bush1, 2);
	this->addChild(bush2, 2);
	this->addChild(bush3, 2);

	 
	building1 = Sprite::create("building1.png");
	building1->setPosition(Vec2(3500, 600));
	building1->setScale(0.83f);
	this->addChild(building1, -1);
	
	building2 = Sprite::create("building2.png");
	building2->setPosition(Vec2(6000, 600));
	building2->setScale(0.83f);
	this->addChild(building2, -1);

	// rickshaw = Sprite::create("rickshaw.png");
	// rickshaw->setPosition(Vec2(8000, 300));
	// rickshaw->setScale(0.5f);
	// rickshaw->setFlipX(true);
	// this->addChild(rickshaw, -1);
	// 
	// auto rickshawPlatform = Sprite::create("platform.png");
	// rickshawPlatform->setScale(1.6f, 0.1f);
	// rickshawPlatform->setRotation(340.0f);
	// rickshawPlatform->setPosition(Vec2(8000, 150));
	// 
	// auto rickshawPlatformBody = PhysicsBody::createBox(ground->getContentSize(), PhysicsMaterial(0, 0, 2));
	// rickshawPlatform->setPhysicsBody(rickshawPlatformBody);
	// rickshawPlatformBody->setDynamic(false);
	// rickshawPlatformBody->setGravityEnable(false);
	// this->addChild(rickshawPlatform, -2);

	ramp = Sprite::create("ramp.png");
	ramp->setPosition(Vec2(8000, 300));
	ramp->setScale(0.7f);
	this->addChild(ramp, -1);

	auto rampPlatform = Sprite::create("platform.png");
	rampPlatform->setScale(1.6f, 0.1f);
	rampPlatform->setRotation(340.0f);
	rampPlatform->setPosition(Vec2(8000, 200));

	auto rampPlatformBody = PhysicsBody::createBox(ground->getContentSize(), PhysicsMaterial(0, 0, 2));
	rampPlatform->setPhysicsBody(rampPlatformBody);
	rampPlatformBody->setDynamic(false);
	rampPlatformBody->setGravityEnable(false);
	this->addChild(rampPlatform, -2);

	trap1 = Sprite::create("trap1.png");
	trap1->setPosition(Vec2(8400, 200));
	trap1->setScale(1.3f);
	this->addChild(trap1, 3);

	auto trapBody = PhysicsBody::createBox(trap1->getContentSize(), PhysicsMaterial(0, 0, 2));
	trap1->setPhysicsBody(trapBody);
	trapBody->setDynamic(false);
	trapBody->setGravityEnable(false);

	trap1->getPhysicsBody()->setCategoryBitmask(0x02);
	trap1->getPhysicsBody()->setCollisionBitmask(0x02);

	trap2 = Sprite::create("trap1.png");
	trap2->setPosition(Vec2(trap1->getContentSize().width + 8400, 200));
	trap2->setScale(1.3f);
	this->addChild(trap2, 3);

	auto trapBody2 = PhysicsBody::createBox(trap2->getContentSize(), PhysicsMaterial(0, 0, 2));
	trap2->setPhysicsBody(trapBody2);
	trapBody2->setDynamic(false);
	trapBody2->setGravityEnable(false);

	trap2->getPhysicsBody()->setCategoryBitmask(0x02);
	trap2->getPhysicsBody()->setCollisionBitmask(0x02);

	trap3 = Sprite::create("trap1.png");
	trap3->setPosition(Vec2((trap1->getContentSize().width * 2) + 8400, 200));
	trap3->setScale(1.3f);
	this->addChild(trap3, 3);

	auto trapBody3 = PhysicsBody::createBox(trap3->getContentSize(), PhysicsMaterial(0, 0, 2));
	trap3->setPhysicsBody(trapBody3);
	trapBody3->setDynamic(false);
	trapBody3->setGravityEnable(false);

	trap3->getPhysicsBody()->setCategoryBitmask(0x02);
	trap3->getPhysicsBody()->setCollisionBitmask(0x02);


	auto rotateAction = RotateBy::create(0.5, 360);
	auto rotateAction2 = RotateBy::create(0.5, 360);
	auto rotateAction3 = RotateBy::create(0.5, 360);
	
	trap1->runAction(RepeatForever::create(rotateAction));
	trap2->runAction(RepeatForever::create(rotateAction2));
	trap3->runAction(RepeatForever::create(rotateAction3));

	S1 = new Searcher(this, Vec2(0,0), 10.0f);

	//Ob1 = new Obstacle(this, Vec2(400, 500), 125, 80, 70);


	Vector<SpriteFrame*> animFramesRight;
	animFramesRight.reserve(8);
	animFramesRight.pushBack(SpriteFrame::create("running\\p1.png", Rect(0, 0, 355, 389)));
	animFramesRight.pushBack(SpriteFrame::create("running\\p2.png", Rect(0, 0, 426, 359)));
	animFramesRight.pushBack(SpriteFrame::create("running\\p3.png", Rect(0, 0, 323, 358)));
	animFramesRight.pushBack(SpriteFrame::create("running\\p4.png", Rect(0, 0, 385, 351)));
	animFramesRight.pushBack(SpriteFrame::create("running\\p5.png", Rect(0, 0, 390, 333)));
	animFramesRight.pushBack(SpriteFrame::create("running\\p6.png", Rect(0, 0, 413, 360)));
	animFramesRight.pushBack(SpriteFrame::create("running\\p7.png", Rect(0, 0, 332, 366)));
	animFramesRight.pushBack(SpriteFrame::create("running\\p8.png", Rect(0, 0, 374, 337)));

	animationRight = Animation::createWithSpriteFrames(animFramesRight, 0.1f);
	runRight = Animate::create(animationRight);

	_player.sprite->runAction(RepeatForever::create(runRight));

//	_player.sprite->runAction(RepeatForever::create(runRight));
	//_player.sprite->runAction(Sequence::create(runRight, nullptr));


	Vector<SpriteFrame*> animFramesStand;
	animFramesStand.reserve(7);
	animFramesStand.pushBack(SpriteFrame::create("standing\\1.png", Rect(0, 0, 450, 450)));
	animFramesStand.pushBack(SpriteFrame::create("standing\\2.png", Rect(0, 0, 450, 450)));
	animFramesStand.pushBack(SpriteFrame::create("standing\\3.png", Rect(0, 0, 450, 450)));
	animFramesStand.pushBack(SpriteFrame::create("standing\\4.png", Rect(0, 0, 450, 450)));
	animFramesStand.pushBack(SpriteFrame::create("standing\\5.png", Rect(0, 0, 450, 450)));
	animFramesStand.pushBack(SpriteFrame::create("standing\\6.png", Rect(0, 0, 450, 450)));
	animFramesStand.pushBack(SpriteFrame::create("standing\\7.png", Rect(0, 0, 450, 450)));

	animationStand = Animation::createWithSpriteFrames(animFramesStand, 0.1f);
	Stand = Animate::create(animationStand);

	//	_player.sprite->runAction(RepeatForever::create(runStand));
	//_player.sprite->runAction(Sequence::create(Stand, nullptr));

	//_player.sprite->setAnchorPoint(Vec2(0, 0));


	ControllerMovement = Sprite::create("ControllerLeft.png");
	ControllerMovement->setScale(0.25f);
	ControllerMovement->setPosition(Vec2(300, 800));
	this->addChild(ControllerMovement, 2);

	Vector<SpriteFrame*> con;
	con.reserve(2);
	con.pushBack(SpriteFrame::create("ControllerLeft.png", Rect(0, 0,  1246, 833)));
	con.pushBack(SpriteFrame::create("ControllerRight.png", Rect(0, 0, 1246, 833)));

	auto conMove = Animation::createWithSpriteFrames(con, 0.3f);
	auto conMoveAn = Animate::create(conMove);
	ControllerMovement->runAction(RepeatForever::create(conMoveAn));

	Vector<SpriteFrame*> con2;
	con2.reserve(2);
	con2.pushBack(SpriteFrame::create("ControllerJump.png", Rect(0, 0, 1246, 833)));
	con2.pushBack(SpriteFrame::create("ControllerJump(1).png", Rect(0, 0, 1246, 833)));

	auto conMove2 = Animation::createWithSpriteFrames(con2, 0.3f);
	auto conMoveAn2 = Animate::create(conMove2);
	ControllerJump->runAction(RepeatForever::create(conMoveAn2));

	//attack controller animation
	ControllerAttack = Sprite::create("ControllerAttack.png");
	ControllerAttack->setScale(0.25f);
	ControllerAttack->setPosition(Vec2(3500, 900));
	this->addChild(ControllerAttack, 2);

	Vector<SpriteFrame*> con3;

	con3.reserve(2);
	con3.pushBack(SpriteFrame::create("ControllerAttack.png", Rect(0, 0, 1246, 833)));
	con3.pushBack(SpriteFrame::create("ControllerJump(1).png", Rect(0, 0, 1246, 833)));

	auto conMoveAn3 = Animate::create(Animation::createWithSpriteFrames(con3, 0.3f));
	ControllerAttack->runAction(RepeatForever::create(conMoveAn3));

	//dash controller animation
	ControllerDash = Sprite::create("ControllerDash.png");
	ControllerDash->setScale(0.25f);
	ControllerDash->setPosition(Vec2(8500, 800));
	this->addChild(ControllerDash, 2);

	Vector<SpriteFrame*> con4;

	con4.reserve(2);
	con4.pushBack(SpriteFrame::create("ControllerDash.png", Rect(0, 0, 1246, 833)));
	con4.pushBack(SpriteFrame::create("ControllerJump(1).png", Rect(0, 0, 1246, 833)));

	auto conMoveAn4 = Animate::create(Animation::createWithSpriteFrames(con4, 0.3f));
	ControllerDash->runAction(RepeatForever::create(conMoveAn4));

	
	auto emitter = ParticleRain::create();
	//emitter->setPosition(_player.sprite->getPositionX() - 500, _player.sprite->getPositionY() + 1000);
	emitter->setPosition(Vec2(800,1200));
	emitter->setGravity(Vec2(0,-1000));
	emitter->setSpeed(300.0f);
	emitter->setScale(0.9f);
	//emitter->setRotation(315);
	emitter->setTotalParticles(5000);

	this->addChild(emitter, -1);

	blood = ParticleSystemQuad::create("blood2.plist");
	//blood->setDuration(-1.0f);
//	blood->setGravity(Vec2(-100, 0));
	//blood->setPosition(Vec2(1000, 500));
	//blood->stopSystem();
	//blood->stopSystem();
	this->addChild(blood, 10);

	fire = ParticleFire::create();
	fire->setDuration(-1.0f);
	this->addChild(fire, 10);

	for (int i = 0; i < 3;i++)
	{
		spawnShuriken();
	}

	this->scheduleUpdate();
	return true;
}

void HelloWorld::Controls()
{
	xInput::Stick lStick, rStick;
	XBoxController.GetSticks(0, lStick, rStick);

	//jump right diagonal
	if ((lStick.xAxis > 0 && lStick.yAxis > 0) && XBoxController.GetButton(0, xInput::Button::A) && jump == false)
	{
		coolDownTime = gameTime + 1.0f;
		jump = true;
		_player.playerBody->setVelocity(Vec2(200, 1000));
	}
	//jump left diagonal
	else if ((lStick.xAxis < 0 && lStick.yAxis > 0) && XBoxController.GetButton(0, xInput::Button::A) && jump == false)
	{
		coolDownTime = gameTime + 1.0f;
		jump = true;
		_player.playerBody->setVelocity(Vec2(-200, 1000));
	}
	//move right
	else if (lStick.xAxis > 0 && _player.sprite->getPositionX() < bg->getContentSize().width * 2 + 5350)
	{
		if (_player.sprite->getScaleX() != -0.7f)
		{
			_player.sprite->setFlipX(false);
			right = true;
			left = false;
		} 

		_player.sprite->setPosition(Vec2(_player.sprite->getPositionX() + 10, _player.sprite->getPositionY()));
	}
	//move left
	else if (lStick.xAxis < 0 && _player.sprite->getPositionX() > 0)
	{
		if (_player.sprite->getScaleX() != -0.7f)
		{
			_player.sprite->setFlipX(true);
			left = true;
			right = false;
		}
		_player.sprite->setPosition(Vec2(_player.sprite->getPositionX() - 10, _player.sprite->getPositionY()));
	}
	//jump verticle
	else if (XBoxController.GetButton(0, xInput::Button::A) && (jump == false))
	{
		coolDownTime = gameTime + 1.0f;
		jump = true;
		_player.playerBody->setVelocity(Vec2(0, 1000));
	}
	//dash
	else if (XBoxController.GetButton(0, xInput::Button::X) && (dash == false) && bar2->getScaleX() >= 25.0f)
	{
		coolDownTime = gameTime + 1.0f;
		dash = true;
		if (right == true)
		{
			_player.playerBody->setVelocity(Vec2(2300, 0));
			bar2->setScaleX(bar2->getScaleX() - 25.0f);
		}
		else if (left == true)
		{
			_player.playerBody->setVelocity(Vec2(-2300, 0));
			bar2->setScaleX(bar2->getScaleX() - 25.0f);
		}
	}
	//crouch
	else if (XBoxController.GetButton(0, xInput::Button::B))
	{
		if (_player.sprite->getScaleY() == 0.3f)
		{
			_player.sprite->setScaleY(0.7f);
		}
		else if (_player.sprite->getScaleY() == 0.7f)
		{
			_player.sprite->setScaleY(0.3f);
		}
	}
	//throw shuriken
	else if (XBoxController.GetButton(0, xInput::Button::LB) && threw == false)
	{
		auto action = MoveBy::create(5, Vec2(20000, 0));
		auto action2 = RepeatForever::create(RotateBy::create(0.5, 360));
		if (shurikenLeft == 3)
		{
			throwTime = gameTime + 2.0f;
			shurikenList[0].sprite->setPosition(_player.sprite->getPositionX() + 70, _player.sprite->getPositionY() + 170);
			shurikenList[0].sprite->runAction(Sequence::createWithTwoActions(action, action2));
			shurikenLeft = shurikenLeft - 1;
			threw = true;
		}
		else if (shurikenLeft == 2)
		{
			throwTime = gameTime + 2.0f;
			shurikenList[1].sprite->setPosition(_player.sprite->getPositionX() + 70, _player.sprite->getPositionY() + 170);
			shurikenList[1].sprite->runAction(Sequence::createWithTwoActions(action, action2));
			shurikenLeft = shurikenLeft - 1;
			threw = true;

		}
		else if (shurikenLeft == 1)
		{
			throwTime = gameTime + 2.0f;
			shurikenList[2].sprite->setPosition(_player.sprite->getPositionX() + 70, _player.sprite->getPositionY()+170);
			shurikenList[2].sprite->runAction(Sequence::createWithTwoActions(action, action2));
			shurikenLeft = shurikenLeft - 1;
			threw = true;
		}
	}
	//attack
	else if (XBoxController.GetButton(0, xInput::Button::RB) && attack == false  && (bar2->getScaleX() >= 20.0f))
	{
		attackTime = gameTime + 0.2;
		attack = true;
		_player.sprite->setTexture("slash.png");
		bar2->setScaleX(bar2->getScaleX() - 20.0f);

		//audio->playBackgroundMusic("battleMusic.mp3");
		for (int i = 0; i < 4;i++)
		{
			if (_player.sprite->getBoundingBox().intersectsRect(enemyList[i].sprite->getBoundingBox()) && right == true)
			{
				blood->setPosition(Vec2(enemyList[i].sprite->getPositionX()+150, enemyList[i].sprite->getPositionY()+200));
				blood->setGravity(Vec2(1000, 0));
				blood->resetSystem();

				XINPUT_VIBRATION rumbler;
				rumbler.wLeftMotorSpeed = 16000;
				rumbler.wRightMotorSpeed = 16000;
				XInputSetState(0, &rumbler);

				enemyList[i].enemyBody->setVelocity(Vec2(200, 0));
				_player.Attack(enemyList[i], 10);

				rumbleTime = gameTime + 0.3f;
			}
		}

		if (_player.sprite->getBoundingBox().intersectsRect(bossSprite->getBoundingBox()) && right == true)
		{
			XINPUT_VIBRATION rumbler;
			rumbler.wLeftMotorSpeed = 16000;
			rumbler.wRightMotorSpeed = 16000;
			XInputSetState(0, &rumbler);

			bossBody->setVelocity(Vec2(200, 0));
			//_player.Attack(enemyList[i], 10);
			bossHealth = bossHealth - 10;
			rumbleTime = gameTime + 0.3f;
		}
	}
	//assassination
	for (int i = 0; i < 4; i++)
	{
		if (_player.sprite->getBoundingBox().intersectsRect(enemyList[i].sprite->getBoundingBox()) && left == true && XBoxController.GetButton(0, xInput::Button::RB))
		{
			_player.sprite->setTexture("assassinate.png");

			blood->setPosition(Vec2(enemyList[i].sprite->getPositionX() + 150, enemyList[i].sprite->getPositionY() + 200));
			blood->setGravity(Vec2(-1000, 0));
			blood->resetSystem();

			XINPUT_VIBRATION rumbler;
			rumbler.wLeftMotorSpeed = 60000;
			rumbler.wRightMotorSpeed = 60000;
			XInputSetState(0, &rumbler);

			_player.Attack(enemyList[i], 50);

			rumbleTime = gameTime + 0.8f;
		}
	}
	//if (_player.getStamina() == 0)
	//{
	//	attackTime = gameTime + 3.0f;
	//	attack = true;
	//}

	if (gameTime >= coolDownTime)
	{
		jump = false;
		dash = false;
		coolDownTime = 0.0f;
	}

	if (gameTime >= attackTime)
	{
		_player.setStamina(90);
		attack = false;
	}

	if (gameTime >= throwTime)
	{
		threw = false;
		throwTime = 0.0f;
	}

	if (gameTime >= rumbleTime)
	{
		XINPUT_VIBRATION rumblerOFF;
		rumblerOFF.wLeftMotorSpeed = 0;
		rumblerOFF.wRightMotorSpeed = 0;
		XInputSetState(0, &rumblerOFF);
		rumbleTime = 0.0f;
	}
}

void HelloWorld::spawnEnemy()
{
	Enemy _enemy = Enemy(this);
	enemyList.push_back(_enemy);
}

void HelloWorld::spawnShuriken()
{
	Projectile shuriken = Projectile(this);
	shurikenList.push_back(shuriken);
}

void HelloWorld::update(float deltaTime)
{
	gameTime += deltaTime;
	
	if (XBoxController.GetButton(0, xInput::Button::Start) && introDone == false)
	{
		introDone = true;
		auto action = FadeOut::create(3);
		auto action2 = FadeOut::create(3);
		auto action3 = FadeOut::create(3);
		introBG->runAction(action);
		label->runAction(action2);
		label2->runAction(action3);
		//introBG->setPosition(-1000, -1000);
	}

	director->getRunningScene()->runAction(Follow::create(_player.sprite, boundry));
	XBoxController.DownloadPackets(1);	
	
	if (_player.sprite->getPositionY() != -1000)
		HelloWorld::Controls();

	static float value = 0.0f;
	static float speed = 0.03f;
	value += speed;

	//bar stuff
	if (_player.sprite->getPositionX() > 1030)
	{
		UICharacter->setPosition(_player.sprite->getPositionX() - 960, 975);
		bar->setPosition(_player.sprite->getPositionX() - 775, 968);
		bar2->setPosition(_player.sprite->getPositionX() - 780, 948);

	}

	if (bar2->getScaleX() < 90.0f)
		bar2->setScaleX(bar2->getScaleX() + 0.3f);


	// for (int i = 0; i < 10; i++)
	// {
	// 	if (_player.sprite->getBoundingBox().intersectsRect(enemyList[i].sprite->getBoundingBox()) && bar->getScaleX() != 0)
	// 		bar->setScaleX(bar->getScaleX() - 1.0f);
	// }

	if ((_player.sprite->getBoundingBox().intersectsRect(trap1->getBoundingBox()) || _player.sprite->getBoundingBox().intersectsRect(trap2->getBoundingBox()) || _player.sprite->getBoundingBox().intersectsRect(trap3->getBoundingBox())) && bar->getScaleX() != 0)
	{
		bar->setScaleX(bar->getScaleX() - 1.0f);
	}

	if (bar->getScaleX() <= 0.0f)
	{
		_player.sprite->setPositionY(-1000);
		_player.playerBody->setDynamic(false);
		label = Label::createWithTTF("GAME OVER", "fonts/arial.ttf", 200);
		label->setPosition(Vec2(_player.sprite->getPositionX(), 500));
		this->addChild(label, 100);
	}

	//move enemies towards players in certain range
	for (int i = 0; i < 4;i++)
	{
		if (sqrt(pow(enemyList[i].sprite->getPositionX() - _player.sprite->getPositionX(), 2) + pow(enemyList[i].sprite->getPositionY()  - _player.sprite->getPositionY(), 2)) <= 500.0f)
		{
			if (enemyList[i].sprite->getPositionX() > _player.sprite->getPositionX())
			{
				enemyList[i].sprite->setPositionX(enemyList[i].sprite->getPositionX() - 5.0f);
			}

			if (enemyList[i].sprite->getBoundingBox().intersectsRect(_player.sprite->getBoundingBox()))
			{
				int random = rand() % 50;

				if (random == 5)
				{
					enemyList[i].sprite->setTexture("enemySlash.png");
					bar->setScaleX(bar->getScaleX() - 5.0f);
				}
			}
		}
	}

	//health replenish

	if (_player.sprite->getBoundingBox().intersectsRect(heart->getBoundingBox()))
	{
		bar->setScaleX(129.0f);
		heart->setPosition(Vec2(-1000, -1000));
	}
	for (int i = 0; i < 4; i++)
	{
		enemyList[i].checkDead();
	}

	//fire head particle
	fire->setPosition(Vec2(bossSprite->getPositionX() + 125, bossSprite->getPositionY() + 385));

	if (bossHealth <= 0)
	{
		bossSprite->setPosition(Vec2(-1000, -1000));
	}
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}