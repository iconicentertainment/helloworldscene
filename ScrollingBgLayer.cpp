#include "ScrollingBgLayer.h"
ScrollingBgLayer::ScrollingBgLayer(float speed)
{
	CCSize visibleSize = CCDirector::sharedDirector() ->getVisibleSize();
	CCSprite* bg = CCSprite::create("bgbg.jpg");
	bg->setPosition(ccp(visibleSize.width* 0.5, visibleSize.height *0.5));
	bg->setScale(4.0f);
	bg->setScaleX(200.0f);
	this->addChild(bg, -1);
	bglayer1 = ScrollingBg::create("bglayer1.png", speed * 0.8, 142);
	bglayer1->setScale(0.9f);
	bglayer1->setPosition(Vec2(0, 0));
	this->addChild(bglayer1, 5);
	bglayer2 = ScrollingBg::create("bglayer2.png", speed * 0.8, 136);
	bglayer2->setScale(0.9f);
	bglayer2->setPosition(Vec2(0, 0));
	this->addChild(bglayer2, 4);
	bglayer3 = ScrollingBg::create("bglayer3.png", speed * 0.5,	0);
	bglayer3->setScale(1.1f);
	bglayer3->setPosition(Vec2(0, 0));
	this->addChild(bglayer3, 3);
	bglayer4 = ScrollingBg::create("bglayer4.png", speed * 0.3, 0);
	bglayer4->setScale(1.1f);
	bglayer4->setPosition(Vec2(0, 0));
	this->addChild(bglayer4, 2);
}
ScrollingBgLayer::~ScrollingBgLayer() {}

void ScrollingBgLayer::update(int direction, float position)
{
	bglayer1->update(direction, position);
	bglayer2->update(direction, position);
	bglayer3->update(direction, position);
	bglayer4->update(direction, position);
}