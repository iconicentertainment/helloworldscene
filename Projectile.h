#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "cocos2d.h"

class Projectile
{
public:
	Projectile(cocos2d::Scene* scene);
	~Projectile();
	cocos2d::Sprite* sprite;
};
#endif // !PROJECTILE_H
