#include "Enemy.h"
Enemy::Enemy(cocos2d::Scene* scene)
{
	sprite = cocos2d::Sprite::create("enemy.png");
	sprite->setScale(0.7f);
	//sprite->setScaleX(-0.7f);

	enemyBody = cocos2d::PhysicsBody::createBox(sprite->getContentSize());
	sprite->setPhysicsBody(enemyBody);
	enemyBody->setVelocity(cocos2d::Vec2(0, 0));
	enemyBody->setRotationEnable(false);

	//sprite->getPhysicsBody()->setCategoryBitmask(0x02);
	//sprite->getPhysicsBody()->setCollisionBitmask(0x02);

	health = 50;

	scene->addChild(sprite);
}
Enemy::~Enemy()
{
	sprite = nullptr;
}
int Enemy::getHealth()
{
	return health;
}

// void Enemy::Attack(Player& player, int damage)
// {
// 	player.health = player.health - damage;
// }

void Enemy::checkDead()
{
	if (health <= 0)
	{
		sprite->setPosition(cocos2d::Vec2(-1000, -1000));
	}
}