#include "Projectile.h"

Projectile::Projectile(cocos2d::Scene* scene)
{
	sprite = cocos2d::Sprite::create("shuriken.png");
	sprite->setScale(0.3f);

	scene->addChild(sprite, -1);
}
Projectile::~Projectile()
{
	sprite = nullptr;
}